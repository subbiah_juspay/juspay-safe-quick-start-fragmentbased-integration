package in.juspay.juspaysafeqs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import in.juspay.godel.analytics.GodelTracker;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.JuspayWebView;
import in.juspay.godel.ui.OnScreenDisplay;
import in.juspay.godel.ui.dialog.JuspayBrandingV2;

/**
 * Created by Veera.Subbiah on 06/10/17.
 */

public class PayActivity extends AppCompatActivity {
    String PAYMENT_URL;
    String PAYMENT_POST_DATA;
    final String transactionId = "JUS_" + (int) (Math.random() * 10000);
    private JuspayBrowserFragment juspayBrowserFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_activity);


        PAYMENT_URL = getString(R.string.payment_url);
        PAYMENT_POST_DATA = getString(R.string.payment_post_data);

        Bundle params = getParams();
        juspayBrowserFragment = new JuspayBrowserFragment();
        juspayBrowserFragment.setArguments(params);
        setupBrowser(juspayBrowserFragment);

        addFragment(juspayBrowserFragment);
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    public Bundle getParams() {
        Bundle args = new Bundle();

        // client identification
        args.putString("merchantId", "juspay_recharge");
        args.putString("clientId", "juspay_recharge_android");
        args.putString("transactionId", transactionId);

        // customer identification
        args.putString("customerId", "9620917775");
        args.putString("customerEmail", "gautam@juspay.in");
        args.putString("customerPhoneNumber", "9620917775");

        // order meta information
        args.putString("displayNote", "Recharge 9620917775 with RS 10");
        args.putString("remarks", "Recharge 9620917775");
        args.putString("lastSixCardDigits","123456");

        // authentication data
        args.putString("url", PAYMENT_URL);
        args.putString("postData", PAYMENT_POST_DATA);

        // Setting properties
        args.putBoolean("customBrandingEnabled", true);
        args.putString("customBrandingVersion", "v2");

        //Setting Custom Headers
        Map<String, String> customHeaders = new HashMap<String, String>();
        customHeaders.put("Accept-Encoding", "gzip, deflate, sdch");
        customHeaders.put("Accept-Language", "en-US,en;q=0.8");
        args.putSerializable("customHeaders", (Serializable) customHeaders);
        args.putSerializable("onScreenDisplay", this.getOnScreenDisplay());

        return args;
    }

    public void setupBrowser(JuspayBrowserFragment browser) {
        // WebView Callback Setup
        JuspayBrowserFragment.JuspayWebviewCallback juspayWebviewCallback = new JuspayBrowserFragment.JuspayWebviewCallback() {
            @Override
            public void webviewReady() {
                JuspayWebView webView = juspayBrowserFragment.getWebView();
                webView.setWebViewClient(new CustomWebViewClient(webView, juspayBrowserFragment, PayActivity.this));
            }
        };
        browser.setupJuspayWebviewCallbackInterface(juspayWebviewCallback);


        // Backbutton Handling Setup
        JuspayBrowserFragment.JuspayBackButtonCallback juspayBackButtonCallback = new JuspayBrowserFragment.JuspayBackButtonCallback() {
            @Override
            public void transactionCancelled(JSONObject jsonObject) throws JSONException {
                GodelTracker.getInstance().trackPaymentStatus("Demo", GodelTracker.CANCELLED);

                Toast.makeText(PayActivity.this, "Payment Cancelled", Toast.LENGTH_LONG).show();
                finishPayments();
            }
        };
        browser.setupJuspayBackButtonCallbackInterface(juspayBackButtonCallback);


        // Custom Branding Setup
        browser.setupBrandingInterface(new JuspayBrandingV2() {
            @Override
            public int createStartWaitingDialogWithLayout() {
                return R.layout.merchant_specific_waiting_dialog_start;
            }

            @Override
            public int createEndWaitingDialogWithLayout() {
                return R.layout.merchant_specific_waiting_dialog_end;
            }

            @Override
            public void onStartWaitingDialogCreated(Dialog dialog) {
                ImageView waiting_logo = dialog.findViewById(in.juspay.godel.R.id.waiting_dialog_logo);
                // Step1 : create the  RotateAnimation object
                RotateAnimation anim = new RotateAnimation(0f, 350f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                // Step 2:  Set the Animation properties
                anim.setInterpolator(new LinearInterpolator());
                anim.setRepeatCount(Animation.INFINITE);
                anim.setDuration(2100);
                anim.setFillEnabled(true);
                anim.setFillAfter(true);
                waiting_logo.startAnimation(anim);
            }

            @Override
            public void onEndWaitingDialogCreated(Dialog dialog) {

            }
        });
    }

    void finishPayments() {
        removeFragment(juspayBrowserFragment);

        finish();
        // Any other custom code
    }



    private void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        /**
         * shouldShowJuspayCloseDialog:
         *  - true: If true, then JusPay will show confirmation dialog for the user
         *  - false: If false, then your app should take care of confirmation from the user for
         *       cancelling the transaction.
         */
        juspayBrowserFragment.juspayBackPressedHandler(true);

        // Your code to show a dialog box if you are passing false.
    }

    public Serializable getOnScreenDisplay() {
        // OSD Setup
        return OnScreenDisplay.Generic("Travel Amount")
                .appendTripleLine("Vimal Kumar", "Koromangala WhiteField", "Travel Time 140 minutes")
                .appendDottedDivider()
                .appendDoubleLine("Cab Number", "KA01AG1337")
                .appendDoubleLine("Cab", "Toyota Etios")
                .setAmount("540")
                .build();
    }
}
