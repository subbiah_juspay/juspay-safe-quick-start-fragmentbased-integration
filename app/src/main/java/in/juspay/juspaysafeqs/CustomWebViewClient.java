package in.juspay.juspaysafeqs;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.juspay.godel.analytics.GodelTracker;
import in.juspay.godel.browser.JuspayWebViewClient;
import in.juspay.godel.ui.JuspayBrowserFragment;

/**
 * Created by Veera.Subbiah on 06/10/17.
 */

class CustomWebViewClient extends JuspayWebViewClient {

    private final PayActivity activity;

    CustomWebViewClient(WebView webView, JuspayBrowserFragment browserFragment, PayActivity homeActivity) {
        super(webView, browserFragment);
        this.activity = homeActivity;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);

        Pattern pattern = Pattern.compile(".*icicibank.*");
        Matcher m = pattern.matcher(url);

        if(m.matches()) {
            GodelTracker.getInstance().trackPaymentStatus(activity.transactionId, GodelTracker.SUCCESS);
            activity.finishPayments();

            Toast.makeText(activity, "Payment Done", Toast.LENGTH_LONG).show();
        }
    }
}
